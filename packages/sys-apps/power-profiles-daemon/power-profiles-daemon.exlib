# Copyright 2021 Florentin Dubois <florentin.dubois@hey.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" user="hadess" suffix="tar.bz2" new_download_scheme=true ] \
    test-dbus-daemon meson \
    systemd-service

export_exlib_phases src_test

SUMMARY="Makes power profiles handling available over D-Bus."
LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="( providers: eudev systemd ) [[ number-selected = exactly-one ]]"
DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-python/dbus-python
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        gnome-desktop/libgudev
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        sys-apps/upower
        sys-auth/polkit:1
    test:
        dev-libs/umockdev
"

MESON_SRC_CONFIGURE_OPTIONS=(
    "providers:systemd -Dsystemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}"
    "providers:eudev -Dsystemdsystemunitdir="
)

power-profiles-daemon_src_test() {
    esandbox allow_net "unix:${TEMP%/}"

    test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net "unix:${TEMP%/}"
}
