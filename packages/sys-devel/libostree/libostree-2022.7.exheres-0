# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ostreedev project=ostree suffix=tar.xz release=v${PV} ]
require bash-completion systemd-service
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Operating system and container binary deployment and upgrades"
HOMEPAGE="https://ostree.readthedocs.org/en/latest/"

LICENCES="LGPL-2"
SLOT="1"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    avahi
    gobject-introspection
    gtk-doc
    systemd
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libsodium[>=1.0.14]
        dev-libs/libxslt
        sys-devel/bison
        sys-devel/libtool
        sys-fs/e2fsprogs
        virtual/pkg-config[>=0.16]
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        app-arch/libarchive[>=2.8.0]
        app-arch/xz[>=5.1.0]
        app-crypt/gpgme[>=1.1.8]
        dev-libs/glib:2[>=2.66.0]
        dev-libs/libgpg-error
        gnome-desktop/libsoup:2.4[>=2.39.1]
        net-misc/curl[>=7.29.0]
        sys-apps/util-linux[>=2.23.0]
        sys-fs/fuse:3[>=3.1.1]
        sys-libs/zlib
        avahi? ( net-dns/avahi[>=0.6.31] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.34.0] )
        providers:gnutls? ( dev-libs/gnutls[>=3.5.0] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.0.1] )
        systemd? ( sys-apps/systemd )
        !sys-devel/ostree [[
            description = [ project renamed upstream ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/musl.patch
    "${FILES}"/${PNV}-lib-repo-checkout-Add-ALLPERMS-for-musl.patch
    "${FILES}"/${PNV}-tests-Handle-musl-s-ERANGE-mapping.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-systemdsystemunitdir=${SYSTEMDSYSTEMUNITDIR}
    --enable-man
    --enable-rofiles-fuse
    --disable-sanitizers
    --with-curl
    --with-ed25519-libsodium
    --with-libarchive
    --with-libmount
    --with-soup
    --without-builtin-grub2-mkconfig
    --without-dracut
    --without-mkinitcpio
    --without-selinux
    --without-static-compiler
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    'providers:gnutls --with-crypto=gnutls --with-crypto=openssl'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( avahi 'systemd libsystemd' )

# Doesn't play nice with sydbox, requires access to /
RESTRICT="test"

src_prepare() {
    edo sed -e '/INTROSPECTION_SCANNER_ENV/d' -i Makefile-libostree.am
    autotools_src_prepare
}

src_install() {
    default
    keepdir /etc/ostree/remotes.d

    if ! option bash-completion ; then
        edo rm -r "${IMAGE}"/usr/share/bash-completion
    fi
}

