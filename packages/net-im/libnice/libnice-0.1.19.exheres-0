# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="GLib ICE implementation"
HOMEPAGE="https://wiki.freedesktop.org/nice/"
DOWNLOADS="https://nice.freedesktop.org/releases/${PNV}.tar.gz"

LICENCES="( LGPL-2.1 MPL-1.1 )"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gstreamer
    gtk-doc
    gupnp
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.54]
        gstreamer? ( media-libs/gstreamer:1.0[>=1.0.0][gobject-introspection?] )
        gupnp? ( net-libs/gupnp-igd:1.0[>=0.2.4][gobject-introspection?] )
        providers:gnutls? ( dev-libs/gnutls[>=2.12.0] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

# Tests need network access
RESTRICT="test"

src_configure() {
    local myconf=(
        -Dexamples=disabled
        $(meson_feature gobject-introspection introspection)
        $(meson_feature gstreamer)
        $(meson_feature gtk-doc gtk_doc)
        $(meson_feature gupnp)
        $(expecting_tests -Dtests=enabled -Dtests=disabled)
    )

    myconf+=(
        -Dcrypto-library=$(option providers:gnutls gnutls openssl)
    )

    exmeson "${myconf[@]}"
}

